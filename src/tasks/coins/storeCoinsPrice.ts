import getPrice from '../../services/coins/getPrice';
import setCoinHistory from '../../services/cache/actions/setCoinHistory';
import { Coin } from '../../helpers/Coin';
import { logger } from '../../submodules/shared-library/utils/logger';

async function storeCoinsPrice () {
    logger.info("Store coins price entered at: " + Date());

    try {

        const date = new Date();

        const steem = await getPrice('hive');
        const sbd = await getPrice('hive_dollar');
        const btc = await getPrice('bitcoin');

        await setCoinHistory(Coin.STEEM, steem.price_usd, date);
        await setCoinHistory(Coin.SBD, sbd.price_usd, date);
        await setCoinHistory(Coin.BTC, btc.price_usd, date);

    } catch(err) {
        logger.error("Store coins price error: ", err);
    }

}

export default storeCoinsPrice;
