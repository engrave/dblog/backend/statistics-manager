import { Statistics } from '../../models/StatisticsModel';
import { Blogs } from '../../submodules/shared-library/models/Blogs';
import { logger } from '../../submodules/shared-library/utils/logger';
import { Users } from "../../submodules/shared-library/models/Users";
import { hive } from '../../submodules/shared-library';

const parseAsset = (asset: string): number => {
    return Number(asset.replace(" SBD", "").replace(" HBD", "").replace(" STEEM", "").replace(" HIVE", ""))
}

async function getAllAccountsStatistics() {

    logger.info("Statistics module entered");

    try {
        const properties = await hive.api.getDynamicGlobalPropertiesAsync();

        const blogs = await Blogs.find({}, {owner: 1});
        const usernames = [...new Set(blogs.map(blog => blog.owner))];

        const accounts = await hive.api.getAccountsAsync(usernames);

        for (const account of accounts) {
            let userStats = await Statistics.findOne({username: account.name});

            if (!userStats) {
                logger.warn(`No user stats in database. Adding new: ${account.name}`);
                userStats = new Statistics({username: account.name});
            }

            const hivePower = hive.formatter.vestToHive(account.vesting_shares, properties.total_vesting_shares, properties.total_vesting_fund_hive ? properties.total_vesting_fund_hive : properties.total_vesting_fund_steem);

            userStats.hbd.push(account.hbd_balance ? parseAsset(account.hbd_balance) : parseAsset(account.sbd_balance));
            userStats.hive.push(parseAsset(account.balance));
            userStats.hive_power.push(hivePower);
            userStats.savings_hive.push(parseAsset(account.savings_balance));
            userStats.savings_hbd.push(account.savings_hbd_balance ? parseAsset(account.savings_hbd_balance) : parseAsset(account.savings_sbd_balance));
            await userStats.save();
        }

    } catch (err) {
        logger.error("Statistics error", err);
    }
}

export default getAllAccountsStatistics;
