import healthApi from "../routes/health/health.routes";
import walletApi from "../routes/wallet/wallet.routes";
import coinsApi from "../routes/coins/coins.routes";
import { endpointLogger } from "../submodules/shared-library/utils/logger";

function routes(app: any) {
    app.use('/health', healthApi);
    app.use('/wallet', endpointLogger, walletApi);
    app.use('/coins', endpointLogger, coinsApi);
}

export default routes;
