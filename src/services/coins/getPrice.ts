import axios from 'axios';
import { logger } from '../../submodules/shared-library';

async function getPrice(coin: string) {

    try {
        const { data } = await axios.get(`https://api.coingecko.com/api/v3/coins/${coin}/`);
        const { current_price, price_change_percentage_24h } = data.market_data;

        logger.info(`${coin}: \$${current_price.usd} (${price_change_percentage_24h}%)`);

        return {
            price_usd: current_price.usd,
            percent_change_24h: price_change_percentage_24h
        }

    } catch (error) {
        return null;
    }

}

export default getPrice;
