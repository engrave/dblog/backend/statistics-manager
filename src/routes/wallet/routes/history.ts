import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import { body, param } from 'express-validator/check';
import { Statistics } from '../../../models/StatisticsModel';

const middleware: any[] = [
    param('username').isString()
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        const [history] = await Statistics
            .find({username: req.params.username})
            .slice('savings_hbd', -30)
            .slice('savings_hive', -30)
            .slice('hbd', -30)
            .slice('hive', -30)
            .slice('hive_power', -30)
            .exec();

        return res.json(history);
    }, req, res);
}

export default {
    middleware,
    handler
}
