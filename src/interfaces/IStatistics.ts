import { Document } from 'mongoose';

export interface IStatistics extends Document {
    steem_username: string;
    hbd: [number];
    hive: [number];
    hive_power: [number];
    savings_hbd: [number];
    savings_hive: [number];
}
