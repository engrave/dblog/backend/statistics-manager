import { Schema, Model, model } from 'mongoose';
import { IStatistics } from '../interfaces/IStatistics';

export let StatisticsSchema = new Schema({
    username: String,
    hbd: [Number],
    hive: [Number],
    hive_power: [Number],
    savings_hbd: [Number],
    savings_hive: [Number],
})

export let Statistics: Model<IStatistics> = model<IStatistics>("statistics", StatisticsSchema);
